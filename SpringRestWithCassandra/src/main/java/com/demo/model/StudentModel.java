package com.demo.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.DataType;
import com.datastax.driver.mapping.annotations.Frozen;

import lombok.Data;

/**
 * This is the model class which is mapped with the cassandra database.
 * @author Manash.Swain
 *
 */
@Table(value = "Student")
@Data
public class StudentModel implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@PrimaryKey
	private int sno;
	private String name;
	private String dept;
	private Set<Long> mobileNo;
	private List<String> email;
	@Frozen
	@CassandraType(userTypeName = "address", type = DataType.Name.UDT)
	private Map<String, Address> addresses;

}
