package com.demo.model;

import java.io.Serializable;

import org.springframework.data.cassandra.core.mapping.UserDefinedType;


import lombok.Data;

/**
 *Address class will represent the User defiend type in the cassandra data base.
 * @author Manash.Swain
 *
 */
@Data
@UserDefinedType(value="address")
public class Address implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String street;
	private String city;
	private String zip;
	private String state;

}
